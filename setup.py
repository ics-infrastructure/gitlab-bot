# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

requirements = ["click", "gidgetlab[aiohttp]"]

test_requirements = [
    "pytest>=3.0.0",
    "pytest-cov",
    "pytest-aiohttp",
    "pytest-asyncio",
    "asynctest",
]

setup(
    name="gitlab-bot",
    author="Benjamin Bertrand",
    author_email="benjamin.bertrand@esss.se",
    description="ESS GitLab bot",
    url="https://gitlab.esss.lu.se/ics-infrastructure/gitlab-bot",
    license="MIT license",
    version="0.3.0",
    install_requires=requirements,
    tests_require=test_requirements,
    packages=find_packages(),
    include_package_data=True,
    keywords="gitlab",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
    ],
    entry_points={"console_scripts": ["gitlab-bot=gitlab_bot.__main__:cli"]},
    extras_require={"tests": test_requirements},
)
