# gitlab-bot

This GitLab bot responds to webhooks from ESS GitLab.

It is used to:

- forward to JIRA GitLab Listener addon push events for group namespaces.
  Other namespaces are filtered out. This is to avoid having duplicate comments in JIRA when forking
  a repository under a personal namespace.

## Configuration

1. Create a system webhook in GitLab triggered for **Push** events.

2. The following environment variables shall be passed to the bot:

   - GL_SECRET: the secret token used when creating the webhook.
   - GL_ACCESS_TOKEN: GitLab access token to access the projects API. Must give access to all projects including
     private ones to check the namespace kind.
   - GITLAB_LISTENER_SECRET: the secret to be sent to JIRA GitLab Listener add-on.
   - GITLAB_BOT_SENTRY_DSN: Sentry DSN to send exceptions (optional)

   This is done by the [ics-ans-role-gitlab-bot](https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-role-gitlab-bot) Ansible role.

## License

MIT
