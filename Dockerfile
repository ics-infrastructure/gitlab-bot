FROM python:3.7-slim

# Create csi user
RUN groupadd -r -g 1000 csi \
  && useradd --no-log-init -m -r -g csi -u 1000 csi

COPY requirements.txt /requirements.txt
RUN python -m venv /venv \
  && . /venv/bin/activate \
  && pip install --no-cache-dir -r /requirements.txt \
  && chown -R csi:csi /venv

COPY --chown=csi:csi . /app/
WORKDIR /app

# Set PYTHONUNBUFFERED to 1 to force the stdout
# and stderr streams to be unbuffered
ENV PATH=/venv/bin:$PATH \
    PYTHONUNBUFFERED=1

USER csi

RUN pip install .

CMD ["gitlab-bot", "run"]
