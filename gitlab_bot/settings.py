import os

GITLAB_LISTENER_URL = os.environ.get(
    "GITLAB_LISTENER_URL", "https://jira.esss.lu.se/plugins/servlet/gitlab/listener"
)
GITLAB_LISTENER_INSTANCE = os.environ.get("GITLAB_LISTENER_INSTANCE", "default")
GITLAB_LISTENER_SECRET = os.environ.get("GITLAB_LISTENER_SECRET", "xxxxxxx")
