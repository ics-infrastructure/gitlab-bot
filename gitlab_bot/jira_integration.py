import logging
import aiohttp
import gidgetlab.routing
from .settings import (
    GITLAB_LISTENER_URL,
    GITLAB_LISTENER_SECRET,
    GITLAB_LISTENER_INSTANCE,
)

router = gidgetlab.routing.Router()
logger = logging.getLogger(__name__)


@router.register("System Hook")
async def jira_integration(event, gl, *args, **kwargs):
    """Forward push events to JIRA

    Only push events for groups are forwarded
    """
    if event.data["event_name"] != "push":
        return
    data = await gl.getitem(f"/projects/{event.data['project_id']}")
    logger.debug(
        f"Namespace of {data['name_with_namespace']} is of type {data['namespace']['kind']}"
    )
    if data["namespace"]["kind"] != "group":
        logger.info(
            f"Not forwarding push event for {data['name_with_namespace']} of type {data['namespace']['kind']}"
        )
        return
    params = {"secret": GITLAB_LISTENER_SECRET, "instance": GITLAB_LISTENER_INSTANCE}
    logger.info(f"Sending push event to {GITLAB_LISTENER_URL}: {event.data}")
    async with aiohttp.ClientSession() as session:
        async with session.post(
            GITLAB_LISTENER_URL, json=event.data, params=params
        ) as resp:
            logger.info(f"GitLab listener response: {resp.status}")
