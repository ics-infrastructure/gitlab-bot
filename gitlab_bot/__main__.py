import os
import logging
import click
import sentry_sdk
from sentry_sdk.integrations.aiohttp import AioHttpIntegration
from gidgetlab.aiohttp import GitLabBot
from . import jira_integration


sentry_dsn = os.environ.get("GITLAB_BOT_SENTRY_DSN")
if sentry_dsn:
    sentry_sdk.init(dsn=sentry_dsn, integrations=[AioHttpIntegration()])
bot = GitLabBot("gitlab-bot", url="https://gitlab.esss.lu.se", wait_consistency=False)
bot.register_routers(jira_integration.router)
logger = logging.getLogger("gitlab_bot")


@click.group()
@click.version_option()
@click.option("--debug/--no-debug", default=False, help="Enable debug logging")
def cli(debug):
    if debug:
        level = logging.DEBUG
    else:
        level = logging.INFO
    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=level
    )
    logger.setLevel(level)


@cli.command()
def run():
    logger.info("Run bot")
    bot.run(access_log=logger)


if __name__ == "__main__":
    cli()
