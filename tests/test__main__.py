from click.testing import CliRunner
from gitlab_bot.__main__ import bot, cli


async def test_dummy_webhook(aiohttp_client):
    """Check that the bot can process a webhook request"""
    client = await aiohttp_client(bot.app)
    headers = {"x-gitlab-event": "Dummy Hook"}
    data = {"msg": "testing webhook request"}
    response = await client.post("/", headers=headers, json=data)
    assert response.status == 200


def test_cli_subcommands():
    runner = CliRunner()
    result = runner.invoke(cli)
    assert result.exit_code == 0
    assert "run" in result.output
