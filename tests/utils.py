"""Utilities for tests"""


class FakeGitLab:
    def __init__(self, *, getiter=None, getitem=None, post=None, put=None):
        self._getitem_return = getitem
        self._getiter_return = getiter
        self._post_return = post
        self._put_return = put
        self.getitem_url = None
        self.getiter_url = None
        self.post_url = []
        self.post_data = []
        self.post_params = []
        self.put_url = []
        self.put_data = []

    async def getitem(self, url):
        self.getitem_url = url
        return self._getitem_return

    async def getiter(self, url):
        self.getiter_url = url
        for item in self._getiter_return:
            yield item

    async def post(self, url, *, data, params=None):
        self.post_url.append(url)
        self.post_data.append(data)
        self.post_params.append(params)
        return self._post_return

    async def put(self, url, *, data):
        self.put_url.append(url)
        self.put_data.append(data)
        return self._put_return
