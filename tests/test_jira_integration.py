import pytest
from asynctest import patch
from gidgetlab import sansio
from gitlab_bot import jira_integration
from gitlab_bot.settings import (
    GITLAB_LISTENER_URL,
    GITLAB_LISTENER_SECRET,
    GITLAB_LISTENER_INSTANCE,
)
from .utils import FakeGitLab


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "event_name",
    (
        "project_create",
        "project_destroy",
        "project_update",
        "user_add_to_team",
        "user_remove_from_team",
        "user_create",
        "user_destroy",
        "user_failed_login",
        "user_rename",
        "key_create",
        "key_destroy",
        "group_create",
        "group_destroy",
        "foo",
    ),
)
async def test_jira_integration_get_project_not_called_on_other_events(event_name):
    """Check that GitLab API is not called on other events"""
    gl = FakeGitLab()
    data = {"event_name": event_name}
    event = sansio.Event(data, event="System Hook")
    await jira_integration.router.dispatch(event, gl)
    # getitem was not called
    assert gl.getitem_url is None


@pytest.mark.asyncio
@patch("aiohttp.ClientSession.post")
async def test_jira_integration_get_project_called_on_push_events(mock_post):
    """Check that GitLab API is called on push events"""
    gl = FakeGitLab(
        getitem={"namespace": {"kind": "user"}, "name_with_namespace": "foo / test"}
    )
    project_id = 3
    data = {"event_name": "push", "project_id": project_id}
    event = sansio.Event(data, event="System Hook")
    await jira_integration.router.dispatch(event, gl)
    # getitem was called
    assert gl.getitem_url == f"/projects/{project_id}"
    # JIRA GitLab listener is not called for non group namespace
    assert mock_post.call_count == 0


@pytest.mark.asyncio
@patch("aiohttp.ClientSession.post")
async def test_jira_integration_gitlab_listener_called_for_groups(mock_post):
    """Check that JIRA GitLab listener is called for groups"""
    gl = FakeGitLab(
        getitem={"namespace": {"kind": "group"}, "name_with_namespace": "foo / test"}
    )
    project_id = 3
    data = {"event_name": "push", "project_id": project_id, "foo": "hello"}
    event = sansio.Event(data, event="System Hook")
    await jira_integration.router.dispatch(event, gl)
    # getitem was called
    assert gl.getitem_url == f"/projects/{project_id}"
    # JIRA GitLab listener is called for group namespace
    assert mock_post.call_count == 1
    params = params = {
        "secret": GITLAB_LISTENER_SECRET,
        "instance": GITLAB_LISTENER_INSTANCE,
    }
    mock_post.assert_called_once_with(GITLAB_LISTENER_URL, json=data, params=params)
